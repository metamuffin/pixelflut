use std::{
    io::{Read, Write},
    net::TcpStream,
    time::Duration,
};

use crate::{color::ColorRGB, coord::Coord};

pub struct Client {
    connection: TcpStream,
    buf: String,
    pending_pixel_requests: usize,
}

pub const CANVAS_W: i32 = 1280;
pub const CANVAS_H: i32 = 720;
pub const CANVAS_W_USIZE: usize = 1280;
pub const CANVAS_H_USIZE: usize = 720;

impl Client {
    pub fn new(addr: &str) -> Self {
        Self {
            connection: TcpStream::connect(addr).unwrap(),
            buf: String::new(),
            pending_pixel_requests: 0,
        }
    }

    pub fn pixel(&mut self, c: Coord, col: &ColorRGB) {
        self.connection
            .write(
                format!(
                    "PX {} {} {:02x}{:02x}{:02x}\n",
                    c.x,
                    c.y,
                    ((col.0 * 255.0) as u8).max(0).min(255),
                    ((col.1 * 255.0) as u8).max(0).min(255),
                    ((col.2 * 255.0) as u8).max(0).min(255)
                )
                .as_bytes(),
            )
            .unwrap();
    }

    pub fn pixel_fill(&mut self, c: Coord, sc: Coord, col: &ColorRGB) {
        for x in c.x..(c.x + sc.x) {
            for y in c.y..(c.y + sc.y) {
                self.pixel(Coord { x, y }, col)
            }
        }
    }

    pub fn request_pixel(&mut self, c: Coord) {
        self.connection
            .write(format!("PX {} {}\n", c.x, c.y).as_bytes())
            .unwrap();
        self.pending_pixel_requests += 1
    }

    pub fn receive_all_responses(&mut self) -> Vec<(Coord, ColorRGB)> {
        let mut o = vec![];
        while self.pending_pixel_requests > 0 {
            o.extend(self.poll_pixel_responses().into_iter());
            std::thread::sleep(Duration::from_millis(5));
        }
        o
    }

    pub fn poll_pixel_responses(&mut self) -> Vec<(Coord, ColorRGB)> {
        let mut o = vec![];
        // todo: this is bad
        let mut buf = [0; 4096];
        let size = self.connection.read(&mut buf).unwrap();
        self.buf += String::from_utf8(buf[0..size].to_vec()).unwrap().as_str();

        let s = self.buf.split("\n").collect::<Vec<_>>();
        for i in 0..(s.len() - 1) {
            let line = s[i];
            let (_, r) = line.split_once(" ").unwrap();
            let (x, r) = r.split_once(" ").unwrap();
            let (y, c) = r.split_once(" ").unwrap();
            let x = x.parse::<i32>().unwrap();
            let y = y.parse::<i32>().unwrap();
            o.push((Coord { x, y }, ColorRGB::from(c).unwrap()));
            self.pending_pixel_requests -= 1;
        }
        self.buf = s[s.len() - 1].to_string();
        return o;
    }

    pub fn read_pixel(&mut self, c: Coord) -> ColorRGB {
        self.request_pixel(c);
        loop {
            for (coord, col) in self.poll_pixel_responses() {
                if coord == c {
                    return col;
                }
            }
            std::thread::sleep(Duration::from_millis(5));
        }
    }
}
