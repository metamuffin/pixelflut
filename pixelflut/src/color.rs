use crate::coord::Blub;
use std::{fmt::Display, str::FromStr};

#[derive(Debug, Clone, Copy)]
pub struct ColorRGB(pub f32, pub f32, pub f32);
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ColorHSV(pub f32, pub f32, pub f32);

impl std::cmp::PartialEq for ColorRGB {
    fn eq(&self, other: &Self) -> bool {
        ((self.0 - other.0).abs() + (self.1 - other.1).abs() + (self.2 - other.2).abs()) < 0.1
    }
}

impl FromStr for ColorRGB {
    type Err = Blub;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ColorRGB::from(s).ok_or(Blub)
    }
}

impl Display for ColorRGB {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{:02x}{:02x}{:02x}",
            ((self.0 * 255.0) as u8).max(0).min(255),
            ((self.1 * 255.0) as u8).max(0).min(255),
            ((self.2 * 255.0) as u8).max(0).min(255)
        ))
    }
}

impl ColorRGB {
    pub fn black() -> Self {
        ColorRGB(0., 0., 0.)
    }
    pub fn white() -> Self {
        ColorRGB(1., 1., 1.)
    }

    pub fn from(s: &str) -> Option<Self> {
        let r = u8::from_str_radix(&s[0..2], 16).ok()?;
        let g = u8::from_str_radix(&s[2..4], 16).ok()?;
        let b = u8::from_str_radix(&s[4..6], 16).ok()?;
        Some(ColorRGB(
            (r as f32) / 255.,
            (g as f32) / 255.,
            (b as f32) / 255.,
        ))
    }
    pub fn invert(&self) -> ColorRGB {
        ColorRGB(1. - self.0, 1. - self.1, 1. - self.2)
    }
}

impl ColorHSV {
    pub fn as_rgb(self) -> ColorRGB {
        let ColorHSV(h, s, v) = self;
        let r;
        let g;
        let b;
        let i;
        let f;
        let p;
        let q;
        let t;
        let h = h % 1.;

        i = (h * 6.).floor() as i8;
        f = h * 6. - i as f32;
        p = v * (1. - s);
        q = v * (1. - f * s);
        t = v * (1. - (1. - f) * s);
        match i % 6 {
            0 => {
                r = v;
                g = t;
                b = p
            }
            1 => {
                r = q;
                g = v;
                b = p
            }
            2 => {
                r = p;
                g = v;
                b = t
            }
            3 => {
                r = p;
                g = q;
                b = v
            }
            4 => {
                r = t;
                g = p;
                b = v
            }
            5 => {
                r = v;
                g = p;
                b = q
            }
            _ => panic!("askjhdaksjhfkjl"),
        }
        ColorRGB(r, g, b)
    }
}
