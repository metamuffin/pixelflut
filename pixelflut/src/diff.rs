use image::{ImageBuffer, Rgb};
use rand::prelude::SliceRandom;

use crate::{
    client::{Client, CANVAS_H, CANVAS_H_USIZE, CANVAS_W, CANVAS_W_USIZE},
    color::ColorRGB,
    coord::Coord,
};

pub type Canvas = Box<[[ColorRGB; CANVAS_H_USIZE]; CANVAS_W_USIZE]>;

pub struct Diff {
    pub shuffle: bool,
    last_frame: Canvas,
    next_frame: Canvas,
}

impl Diff {
    pub fn new() -> Self {
        Self {
            shuffle: false,
            last_frame: new_canvas(),
            next_frame: new_canvas(),
        }
    }

    pub fn pixel(&mut self, c: Coord, col: ColorRGB) {
        if c.x >= 0 && c.x < CANVAS_W && c.y >= 0 && c.y < CANVAS_H {
            self.next_frame[c.x as usize][c.y as usize] = col
        }
    }

    pub fn fill_rect(&mut self, c: Coord, sc: Coord, col: ColorRGB) {
        for x in c.x..(c.x + sc.x) {
            for y in c.y..(c.y + sc.y) {
                self.pixel(Coord { x, y }, col)
            }
        }
    }


    pub fn fill_circle(&mut self, c: Coord, sc: Coord, col: ColorRGB) {
        for x in c.x..(c.x + sc.x) {
            for y in c.y..(c.y + sc.y) {
                self.pixel(Coord { x, y }, col)
            }
        }
    }

    pub fn clear(&mut self) {
        for x in 0..CANVAS_W_USIZE {
            for y in 0..CANVAS_H_USIZE {
                self.next_frame[x][y] = ColorRGB(0.0, 0.0, 0.0);
            }
        }
    }

    pub fn draw_image(&mut self, offset: Coord, image: &ImageBuffer<Rgb<f32>, Vec<f32>>) {
        for x in 0..image.width() {
            for y in 0..image.height() {
                let p = image.get_pixel(x, y);
                self.pixel(
                    Coord {
                        x: x as i32,
                        y: y as i32,
                    } + offset,
                    ColorRGB(p.0[0], p.0[1], p.0[2]),
                )
            }
        }
    }

    pub fn submit(&mut self, client: &mut Client) {
        let mut changes = Vec::<(Coord, ColorRGB)>::new();
        for x in 0..CANVAS_W_USIZE {
            for y in 0..CANVAS_H_USIZE {
                if self.last_frame[x][y] != self.next_frame[x][y] {
                    changes.push((
                        Coord {
                            x: x as i32,
                            y: y as i32,
                        },
                        self.next_frame[x][y],
                    ));
                    self.last_frame[x][y] = self.next_frame[x][y];
                }
            }
        }
        if self.shuffle {
            changes.shuffle(&mut rand::thread_rng());
        }
        for (coord, color) in changes {
            client.pixel(coord, &color)
        }
    }
}

pub fn new_canvas() -> Canvas {
    let boxed_slice =
        vec![[ColorRGB(0.0, 0.0, 0.0); CANVAS_H_USIZE]; CANVAS_W_USIZE].into_boxed_slice();
    let ptr = ::std::boxed::Box::into_raw(boxed_slice)
        as *mut [[ColorRGB; CANVAS_H_USIZE]; CANVAS_W_USIZE];
    unsafe { Box::from_raw(ptr) }
}
