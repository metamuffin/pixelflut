use clap::Parser;
use pixelflut::diff::Diff;
use pixelflut::{client::Client, coord::Coord};

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: 0, y:0 })]
    offset: Coord,
    #[clap(short, long)]
    image: String,
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    let mut diff = Diff::new();
    let image = image::open(args.image).unwrap().to_rgb32f();

    diff.draw_image(args.offset, &image);
    diff.submit(&mut client);
}
