use clap::Parser;
use pixelflut::client::{CANVAS_H, CANVAS_W};
use pixelflut::color::ColorRGB;
use pixelflut::diff::Diff;
use pixelflut::{client::Client, coord::Coord};
use std::str::FromStr;
use std::time::Duration;

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: 100, y:100})]
    origin: Coord,
    #[clap(short, long, default_value_t = Coord {x: 5, y:6 })]
    velocity: Coord,
    #[clap(short, long, default_value_t = Coord {x: 20, y: 20})]
    size: Coord,
    #[clap(short = 'S', long, parse(try_from_str), default_value = "false")]
    shuffle: bool,
    #[clap(parse(try_from_str), default_value = "ff0000/00ff00/0000ff")]
    colors: String,
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    let mut diff = Diff::new();
    let mut pos = Coord::ORIGIN;
    let mut vel = args.velocity;
    diff.shuffle = args.shuffle;

    let colors = args
        .colors
        .split("/")
        .map(|e| ColorRGB::from_str(e).unwrap())
        .collect::<Vec<_>>();

    for i in 0.. {
        pos += vel;

        diff.fill_rect(pos, args.size, colors[i % colors.len()]);

        if pos.x < 0 || pos.x + args.size.x as i32 >= CANVAS_W {
            vel.x *= -1;
        }
        if pos.y < 0 || pos.y + args.size.y as i32 >= CANVAS_H {
            vel.y *= -1;
        }

        if i % 5 == 0 {
            diff.submit(&mut client);
            std::thread::sleep(Duration::from_nanos(1_000_000 / 30));
        }
    }
}
