use clap::Parser;
use pixelflut::color::ColorRGB;
use pixelflut::diff::Diff;
use pixelflut::{client::Client, coord::Coord};
use std::io::{stdin, Read};

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: 100, y:100})]
    origin: Coord,
    #[clap(long, parse(try_from_str), default_value = "false")]
    shuffle: bool,
    #[clap(short, long, default_value_t = Coord {x: 100, y:100})]
    size: Coord,
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    let mut diff = Diff::new();
    diff.shuffle = args.shuffle;

    let buf_size = args.size.x * args.size.y * 3;
    let mut frame_buffer = Vec::with_capacity(buf_size as usize);
    loop {
        frame_buffer.clear();
        let mut frame_reader = stdin().take(buf_size as u64);
        let bytes_read = frame_reader.read_to_end(&mut frame_buffer).unwrap();
        if bytes_read == 0 {
            break;
        }

        for x in 0..args.size.x {
            for y in 0..args.size.y {
                let offset = 3 * (y * args.size.x + x) as usize;
                let color = ColorRGB(
                    frame_buffer[offset + 0] as f32 / 255.0,
                    frame_buffer[offset + 1] as f32 / 255.0,
                    frame_buffer[offset + 2] as f32 / 255.0,
                );
                diff.pixel(Coord { x, y }, color);
            }
        }

        diff.submit(&mut client);
    }
}
