use std::time::Instant;

use clap::Parser;
use pixelflut::color::ColorRGB;
use pixelflut::diff::Diff;
use pixelflut::{client::Client, coord::Coord};
use rand::prelude::SliceRandom;

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: 100, y:100})]
    size: Coord,
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Particle {
    color: ColorRGB,
    density: f32,
    movable: bool,
    flammable: bool,
    diffuse: bool,
    temp: f32,
}

impl Particle {
    pub fn sand() -> Self {
        Self {
            color: ColorRGB(1.0, 0.5, 0.0),
            density: 2.0,
            movable: true,
            flammable: true,
            temp: 30.0,
            diffuse: false,
        }
    }
    pub fn stone() -> Self {
        Self {
            color: ColorRGB(0.5, 0.5, 0.5),
            density: 10.0,
            movable: false,
            flammable: false,
            temp: 10.0,
            diffuse: false,
        }
    }
    pub fn air() -> Self {
        Self {
            color: ColorRGB(0.0, 0.0, 0.0),
            density: 1.0,
            movable: true,
            flammable: false,
            temp: 20.0,
            diffuse: false,
        }
    }
    pub fn fire() -> Self {
        Self {
            color: ColorRGB(1.0, 0.3, 0.3),
            density: 0.9,
            movable: true,
            flammable: false,
            temp: 500.0,
            diffuse: true,
        }
    }
}

struct ParticleCanvas {
    air: Particle,
    canvas: Vec<Vec<Particle>>,
    width: i32,
    height: i32,
}
impl ParticleCanvas {
    pub fn new() -> Self {
        let (width, height) = (200, 200);
        let canvas = (0..width)
            .map(|_| (0..height).map(|_| Particle::air()).collect::<Vec<_>>())
            .collect::<Vec<_>>();
        Self {
            canvas,
            width,
            height,
            air: Particle::air(),
        }
    }
    pub fn get(&mut self, coord: Coord) -> &mut Particle {
        if coord.x >= 0 && coord.x < self.width && coord.y >= 0 && coord.y < self.height {
            &mut self.canvas[coord.x as usize][coord.y as usize]
        } else {
            self.air = Particle::air();
            &mut self.air
        }
    }
    pub fn swap(&mut self, a: Coord, b: Coord) {
        let t = *self.get(a);
        *self.get(a) = *self.get(b);
        *self.get(b) = t;
    }
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    let mut diff = Diff::new();
    let mut canvas = ParticleCanvas::new();

    for x in 0..40 {
        for y in 0..40 {
            *canvas.get(Coord { x, y }) = Particle::sand();
        }
    }
    for x in 0..40 {
        for y in 50..80 {
            *canvas.get(Coord { x, y }) = Particle::stone();
        }
    }
    for x in 30..50 {
        for y in 100..120 {
            *canvas.get(Coord { x, y }) = Particle::fire();
        }
    }

    loop {
        let start_iter = Instant::now();

        for x in 0..canvas.width {
            for y in 0..canvas.height {
                let pos = Coord { x, y };
                let part = *canvas.get(pos);
                let mut prio = [0, -1, 1];
                prio.shuffle(&mut rand::thread_rng());

                {
                    let mut p = *canvas.get(pos);
                    let env_temp = 0.25
                        * (canvas.get(pos + Coord { x: 0, y: -1 }).temp
                            + canvas.get(pos + Coord { x: 0, y: 1 }).temp
                            + canvas.get(pos + Coord { x: 1, y: 0 }).temp
                            + canvas.get(pos + Coord { x: -1, y: 0 }).temp);
                    p.temp += (env_temp - p.temp) * 0.1;
                    *canvas.get(pos) = p;
                }

                if part.flammable {
                    if part.temp > 200.0 {
                        *canvas.get(pos) = Particle::fire()
                    }
                }

                if part.movable {
                    let offx = *[0, -1, 1].choose(&mut rand::thread_rng()).unwrap();
                    let off = Coord { x: offx, y: 1 };
                    let part2 = *canvas.get(pos + off);
                    if part2.movable && part2.density < part.density {
                        canvas.swap(pos, pos + off);
                    }
                }

                if part.diffuse {
                    let x = *[-1, 0, 1].choose(&mut rand::thread_rng()).unwrap();
                    let y = *[-1, 0, 1].choose(&mut rand::thread_rng()).unwrap();
                    let off = Coord { x, y };
                    let part2 = *canvas.get(pos + off);
                    if part2.movable {
                        canvas.swap(pos, pos + off);
                    }
                }
            }
        }

        diff.clear();
        for x in 0..canvas.width {
            for y in 0..canvas.height {
                diff.pixel(Coord { x, y }, canvas.get(Coord { x, y }).color)
            }
        }

        let end_iter = Instant::now();
        print!("{}\n\x1b[A", (end_iter - start_iter).as_millis());

        diff.submit(&mut client);
    }
}
