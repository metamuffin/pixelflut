use clap::Parser;
use pixelflut::client::{CANVAS_H, CANVAS_W};
use pixelflut::diff::Diff;
use pixelflut::{client::Client, coord::Coord};
use std::time::Duration;

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: 100, y:100})]
    origin: Coord,
    #[clap(short, long, default_value_t = Coord {x: 5, y:5 })]
    velocity: Coord,
    #[clap(short, long)]
    image: String,
    #[clap(short, long, parse(try_from_str), default_value = "true")]
    clear: bool,
    #[clap(short, long, parse(try_from_str), default_value = "false")]
    shuffle: bool,
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    let mut diff = Diff::new();
    let mut pos = Coord::ORIGIN;
    let mut vel = args.velocity;
    let image = image::open(args.image).unwrap().to_rgb32f();
    diff.shuffle = args.shuffle;

    loop {
        pos += vel;
        if args.clear {
            diff.clear();
        }
        diff.draw_image(pos, &image);

        if pos.x < 0 || pos.x + image.width() as i32 >= CANVAS_W {
            vel.x *= -1;
        }
        if pos.y < 0 || pos.y + image.height() as i32 >= CANVAS_H {
            vel.y *= -1;
        }

        diff.submit(&mut client);
        std::thread::sleep(Duration::from_nanos(1_000_000 / 30));
    }
}
