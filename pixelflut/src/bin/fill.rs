use clap::Parser;
use pixelflut::client::{CANVAS_H, CANVAS_W};
use pixelflut::{client::Client, color::ColorRGB, coord::Coord};

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, default_value_t = std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())]
    addr: String,
    #[clap(short, long, default_value_t = Coord {x: CANVAS_W, y: CANVAS_H})]
    size: Coord,
    #[clap(short, long, default_value_t = Coord {x: 0, y: 0})]
    offset: Coord,
    #[clap(short, long, default_value_t = ColorRGB(0.0, 0.0, 0.0))]
    color: ColorRGB,
}

fn main() {
    let args = Args::parse();
    let mut client = Client::new(args.addr.as_str());

    client.pixel_fill(args.offset, args.size, &args.color)
}
