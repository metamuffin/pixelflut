#!/bin/fish

set CAPTURE_RES 1920x1080
set TARGET_RES "$argv[1]"

ffmpeg -r 30 -y -hwaccel vaapi -vaapi_device /dev/dri/card0 -device /dev/dri/card0 -f kmsgrab -i - -vf 'hwmap=derive_device=vaapi,hwdownload,format=bgr0' -f rawvideo -pixel_format bgr0 pipe:1 |
    ffmpeg -f rawvideo -pixel_format bgr0 -video_size "$CAPTURE_RES" -i pipe:0 -vf "scale=$TARGET_RES,format=rgb24" -f rawvideo pipe:1 |
    cargo run --bin video --release -- -s "$TARGET_RES" --shuffle true
