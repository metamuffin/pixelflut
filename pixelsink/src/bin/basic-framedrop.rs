use std::{
    io::{stdin, BufWriter, Read, Write},
    mem::swap,
    net::TcpStream,
    sync::mpsc::channel,
    thread,
};

fn main() {
    let mut conn = BufWriter::new(TcpStream::connect("pixelflut.uwu.industries:1234").unwrap());
    // let mut conn = BufWriter::new(TcpStream::connect("127.0.0.1:1234").unwrap());

    let (tx, rx) = channel();

    let width = 1280;
    let height = 720;

    thread::spawn(move || loop {
        let mut frame = (0..(height * width * 3)).map(|_| 0u8).collect::<Vec<_>>();
        stdin().read_exact(&mut frame).unwrap();
        tx.send(frame).unwrap();
    });

    let mut frame;
    let mut last_frame = (0..(height * width * 3)).map(|_| 0u8).collect::<Vec<_>>();
    loop {
        frame = rx.recv().unwrap();
        for fr in rx.try_iter() {
            frame = fr;
        }

        for x in 0..width {
            for y in 0..height {
                let i = (x + y * width) * 3;

                let r = frame[i + 0];
                let rl = last_frame[i + 0];
                let g = frame[i + 1];
                let gl = last_frame[i + 1];
                let b = frame[i + 2];
                let bl = last_frame[i + 2];

                let diff = r.abs_diff(rl) as u16 + g.abs_diff(gl) as u16 + b.abs_diff(bl) as u16;
                if diff > 10 {
                    writeln!(conn, "PX {x} {y} {r:02x}{g:02x}{b:02x}").unwrap();
                }
            }
        }
        conn.flush().unwrap();

        swap(&mut frame, &mut last_frame);
    }
}
