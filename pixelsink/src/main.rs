#![feature(let_chains)]

use std::{
    collections::BinaryHeap,
    io::{stdin, BufWriter, Read, Write},
    net::TcpStream,
    thread,
    time::{Duration, Instant},
};

use crossbeam_channel::{bounded, unbounded};
use rand::random;

fn main() {
    let mut conn = BufWriter::new(TcpStream::connect("pixelflut.uwu.industries:1234").unwrap());
    // let mut conn = BufWriter::new(TcpStream::connect("127.0.0.1:1234").unwrap());

    let (frame_tx, frame_rx) = unbounded();
    let (change_tx, change_rx) = bounded(1024);

    let width = 1280;
    let height = 720;

    thread::spawn(move || loop {
        let mut frame = (0..(height * width * 3)).map(|_| 0u8).collect::<Vec<_>>();
        if stdin().read_exact(&mut frame).is_err() {
            eprintln!("---- stream end");
            break;
        }
        frame_tx.send(frame).unwrap();
    });
    thread::spawn(move || {
        for (x, y, r, g, b) in change_rx.iter() {
            writeln!(conn, "PX {x} {y} {r:02x}{g:02x}{b:02x}").unwrap();
        }
    });

    let mut target_frame = (0..(height * width * 3)).map(|_| 0u8).collect::<Vec<_>>();
    let mut written_frame = (0..(height * width * 3)).map(|_| 0u8).collect::<Vec<_>>();
    let mut t = Instant::now();
    let mut prio = BinaryHeap::new();

    let mut change_limit = 0;

    loop {
        for fr in frame_rx.try_iter() {
            target_frame = fr;
        }

        prio.clear();
        for x in 0..width {
            for y in 0..height {
                let i = (x + y * width) * 3;

                let r = target_frame[i + 0];
                let rl = written_frame[i + 0];
                let g = target_frame[i + 1];
                let gl = written_frame[i + 1];
                let b = target_frame[i + 2];
                let bl = written_frame[i + 2];

                let diff = r.abs_diff(rl) as u16
                    + g.abs_diff(gl) as u16
                    + b.abs_diff(bl) as u16
                    + random::<u8>() as u16 / 2;
                // let diff = r.abs_diff(rl) as u16 + g.abs_diff(gl) as u16 + b.abs_diff(bl) as u16;
                // if diff > 0 {
                prio.push((diff, x, y))
                // }
            }
        }

        let mut changes_sent = 0;

        change_limit = (change_limit * 3) / 2 + 64;

        while let Some((_, x, y)) = prio.pop() {
            let i = (x + y * width) * 3;
            let r = target_frame[i + 0];
            let g = target_frame[i + 1];
            let b = target_frame[i + 2];
            match change_tx.try_send((x, y, r, g, b)) {
                Ok(_) => {
                    written_frame[i + 0] = target_frame[i + 0];
                    written_frame[i + 1] = target_frame[i + 1];
                    written_frame[i + 2] = target_frame[i + 2];
                    changes_sent += 1;
                }
                Err(_) => {
                    change_limit /= 2;
                    break;
                }
            }
            if changes_sent > change_limit {
                break;
            }
        }

        eprintln!("{:?}\tcl={}\t{}", t.elapsed(), change_limit, changes_sent);
        t = Instant::now();

        std::thread::sleep(Duration::from_millis(10));
    }
}
