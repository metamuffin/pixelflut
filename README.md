# pixelflut

My collection of programs for playing pixelflut, including a framework for using the gpu for efficient diffing between frames.

## Usage

The binaries of pixelflut-gpu crate have their usage availible through the `--help` flag.
The rest needs to be looked up in the source code.

## Licence

GNU Affero General Public Licence Version 3. See [LICENSE](./LICENSE)

