#![feature(nonzero_min_max)]
use clap::Parser;
use wgpu::{CommandEncoder, TextureView};
pub mod app;
pub mod client;
pub mod color;
pub mod constants;
pub mod coord;
pub mod diff;
pub mod feedback;
pub mod font;
pub mod helpers;
pub mod stats;
pub mod texture;
pub mod video_source;

pub struct DrawContext {
    pub target: TextureView,
    pub encoder: CommandEncoder,
}

#[derive(Debug, Parser, Clone)]
pub struct CommonArgs {
    /// tcp server address to connect to (env: PIXELFLUT_SERVER_ADDR)
    #[clap(short = 'A', long)]
    pub addr: Option<String>,
    /// threat pixels identical, if the euclidian color distance is less than this value
    #[clap(long, default_value_t = 0.3)]
    pub similarity: f32,
    /// shuffle each batch of changes
    #[clap(short = 'S', long)]
    pub shuffle: bool,
    /// redraw indefinetly
    #[clap(short = 'R', long)]
    pub redraw: bool,
    /// dont optimize batched by only sending the difference
    #[clap(short = 'N', long)]
    pub nodiff: bool,
    /// load feedback from the stream to the last framebuffer
    #[clap(long, parse(try_from_str))]
    pub feedback: Option<String>,
    /// load feedback from env: PIXELFLUT_FEEDBACK_URI
    #[clap(short = 'F', long)]
    pub auto_feedback: bool,
    /// show some statistics in the top left
    #[clap(short = 'V', long, parse(from_occurrences))]
    pub stats: usize,
    /// artificially slows down submission to reduce gpu load
    #[clap(short = 'M', long)]
    fpslimit: Option<f64>,
}
