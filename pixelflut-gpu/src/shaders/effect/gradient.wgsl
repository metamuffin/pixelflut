
struct Uniforms {
    time: f32
};

@group(0) @binding(0) var<uniform> uniforms: Uniforms; 
@group(0) @binding(1) var tex: texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let coords = vec2<i32>(global_id.xy);
    let uv = vec2<f32>(coords) / vec2<f32>(1280.0, 720.0);

    textureStore(tex, coords.xy, vec4<f32>(uv.x, 0.0, uv.y, 1.0));
}
