
struct Uniforms {
    time: f32
};

@group(0) @binding(0) var<uniform> uniforms: Uniforms; 
@group(0) @binding(1) var tex: texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let coords = vec2<i32>(global_id.xy);
    let uv_ = vec2<f32>(coords) / vec2<f32>(1280.0, 720.0);
    let uv = uv_ * 2.0 - 1.0;


    let r = uniforms.time * 0.1;
    let uv__ = vec2<f32>(uv.x * cos(r) + uv.y * -sin(r), uv.x * sin(r) + uv.y * cos(r));

    let col = step(abs(uv__.x)+abs(uv__.y), 0.3);
 
    textureStore(tex, coords.xy, vec4<f32>(vec3<f32>(col), 1.0));
}