
@group(0) @binding(1) var source: texture_2d<f32>;
@group(0) @binding(2) var destination: texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let dimensions = textureDimensions(source);
    let coords = vec2<i32>(global_id.xy);
    // if (coords.x >= dimensions.x || coords.y >= dimensions.y) {
    //     return;
    // }
    textureStore(destination, coords.xy, textureLoad(source, coords.xy, 0));
}
