
struct Uniforms {
    swap: i32,
    similarity: f32,
};

@group(0) @binding(0) var<uniform> uniforms: Uniforms; 
@group(0) @binding(1) var rtex0: texture_2d<f32>;
@group(0) @binding(2) var rtex1: texture_2d<f32>;
@group(0) @binding(3) var diff_texture: texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let dimensions = textureDimensions(rtex0);
    let coords = vec2<i32>(global_id.xy);
    // if (coords.x >= dimensions.x || coords.y >= dimensions.y) {
    //     return;
    // }

    var next: vec4<f32>;
    var old: vec4<f32>;
    if uniforms.swap != 1 {
        next = textureLoad(rtex0, coords.xy, 0);
        old = textureLoad(rtex1, coords.xy, 0);
    } else {
        next = textureLoad(rtex1, coords.xy, 0);
        old = textureLoad(rtex0, coords.xy, 0);
    }


    let d = distance(next, old);
    let send = (1.0 - step(d, uniforms.similarity)) * step(uniforms.similarity * 2.0, next.a);

    textureStore(diff_texture, coords.xy, vec4<f32>(next.rgb, send));
}
