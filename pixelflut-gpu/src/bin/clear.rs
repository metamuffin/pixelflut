use clap::Parser;
use pixelflut_gpu::app::App;
use pixelflut_gpu::color::ColorRGB;
use pixelflut_gpu::CommonArgs;

#[derive(Parser, Debug)]
struct Args {
    #[clap(flatten)]
    common: CommonArgs,
    /// clear color
    #[clap(short, long, parse(try_from_str), default_value = "000000")]
    color: ColorRGB,
}

async fn run() {
    let args = Args::parse();
    let mut a = App::new(&args.common).await;

    loop {
        let mut context = a.get_context();

        context
            .encoder
            .begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &context.target,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: args.color.0 as f64,
                            g: args.color.1 as f64,
                            b: args.color.2 as f64,
                            a: 1.0,
                        }),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

        a.submit(context).await;

        if !args.common.redraw {
            break;
        }
    }
}

fn main() {
    env_logger::init();
    pollster::block_on(run());
}
