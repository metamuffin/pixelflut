use clap::Parser;
use pixelflut_gpu::{
    app::App,
    color::ColorRGB,
    constants::{CANVAS_HEIGHT, CANVAS_WIDTH},
    coord::Coord,
    font::FontRenderer,
    CommonArgs,
};
use wgpu::RenderPassDescriptor;

#[derive(Parser, Debug)]
struct Args {
    #[clap(flatten)]
    common: CommonArgs,
    #[clap(short, long, default_value_t = Coord { x: 10.0, y: 10.0 })]
    origin: Coord,
    #[clap(short, long)]
    text: String,
    #[clap(short, long, parse(try_from_str), default_value = "FFFFFF")]
    color: ColorRGB,
    #[clap(short, long, parse(try_from_str), default_value = "30")]
    size: f32,
    #[clap(short, long)]
    background: Option<ColorRGB>,
}

async fn run() {
    let args = Args::parse();
    let mut a = App::new(&args.common).await;

    let mut font_renderer = FontRenderer::new(&a.device);

    loop {
        let mut context = a.get_context();

        context.encoder.begin_render_pass(&RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &context.target,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(
                        args.background
                            .map(|a| wgpu::Color {
                                r: a.0 as f64,
                                g: a.1 as f64,
                                b: a.2 as f64,
                                a: 1.0,
                            })
                            .unwrap_or(wgpu::Color::TRANSPARENT),
                    ),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        font_renderer.enqueue(wgpu_glyph::Section {
            screen_position: (args.origin.x as f32, args.origin.y as f32),
            bounds: (CANVAS_WIDTH as f32, CANVAS_HEIGHT as f32),
            text: vec![wgpu_glyph::Text::new(args.text.as_str())
                .with_color([args.color.0, args.color.1, args.color.2, 1.0])
                .with_scale(args.size)],
            ..wgpu_glyph::Section::default()
        });

        font_renderer.finish(&a.device, &mut context);

        a.submit(context).await;

        font_renderer.post_render();

        if !args.common.redraw {
            break;
        }
    }
}

fn main() {
    env_logger::init();
    pollster::block_on(run());
}
