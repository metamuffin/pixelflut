use bytemuck::{Pod, Zeroable};
use clap::Parser;
use pixelflut_gpu::app::App;
use pixelflut_gpu::constants::CANVAS_HEIGHT;
use pixelflut_gpu::constants::CANVAS_WIDTH;
use pixelflut_gpu::constants::RENDER_FORMAT;
use pixelflut_gpu::helpers::BufferDimensions;
use pixelflut_gpu::CommonArgs;
use std::borrow::Cow;
use std::fs::File;
use std::io::Read;
use std::time::Instant;
use wgpu::util::DeviceExt;
use wgpu::BindGroupLayoutDescriptor;
use wgpu::BindGroupLayoutEntry;
use wgpu::ImageCopyTexture;
use wgpu::Origin3d;
use wgpu::PipelineLayoutDescriptor;
use wgpu::ShaderStages;

#[derive(Parser, Debug)]
struct Args {
    #[clap(flatten)]
    common: CommonArgs,

    /// path to a wgsl shader. see examples in src/shaders/effects
    shader_path: String,
}

async fn run() {
    let args = Args::parse();
    let mut a = App::new(&args.common).await;

    let mut shader_source = String::new();
    let mut shader_file = File::open(args.shader_path).unwrap();
    shader_file.read_to_string(&mut shader_source).unwrap();

    let shader_module = a.device.create_shader_module(wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(shader_source.as_str())),
    });

    let buffer_dimensions = BufferDimensions::new(CANVAS_WIDTH, CANVAS_HEIGHT);
    let texture_extent = wgpu::Extent3d {
        width: buffer_dimensions.width as u32,
        height: buffer_dimensions.height as u32,
        depth_or_array_layers: 1,
    };

    let temp_target = a.device.create_texture(&wgpu::TextureDescriptor {
        size: texture_extent,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: RENDER_FORMAT,
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT
            | wgpu::TextureUsages::COPY_SRC
            | wgpu::TextureUsages::COPY_DST
            | wgpu::TextureUsages::STORAGE_BINDING
            | wgpu::TextureUsages::TEXTURE_BINDING,
        label: None,
        view_formats: &[],
    });

    let mut uniforms = ShaderUniforms { time: 0.0 };

    let uniform_buffer = a
        .device
        .create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("shader uniform buffer"),
            contents: bytemuck::cast_slice(&[uniforms]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

    let bind_group_layout = a
        .device
        .create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: None,
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::StorageTexture {
                        access: wgpu::StorageTextureAccess::WriteOnly,
                        format: wgpu::TextureFormat::Rgba8Unorm,
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
            ],
        });

    let bind_group = a.device.create_bind_group(&wgpu::BindGroupDescriptor {
        label: Some("shading bind group"),
        layout: &bind_group_layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: uniform_buffer.as_entire_binding(),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::TextureView(
                    &temp_target.create_view(&wgpu::TextureViewDescriptor::default()),
                ),
            },
        ],
    });

    let pipeline = a
        .device
        .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
            label: Some("draw"),
            layout: Some(&a.device.create_pipeline_layout(&PipelineLayoutDescriptor {
                label: None,
                bind_group_layouts: &[&bind_group_layout],
                push_constant_ranges: &[],
            })),
            module: &shader_module,
            entry_point: "main",
        });

    let start = Instant::now();
    loop {
        let mut context = a.get_context();

        {
            uniforms.time = start.elapsed().as_secs_f32();
            a.queue
                .write_buffer(&uniform_buffer, 0, bytemuck::cast_slice(&[uniforms]));
            let mut pass = context
                .encoder
                .begin_compute_pass(&wgpu::ComputePassDescriptor {
                    label: Some("shade"),
                });
            pass.set_pipeline(&pipeline);
            pass.set_bind_group(0, &bind_group, &[]);
            pass.dispatch_workgroups(CANVAS_WIDTH as u32, CANVAS_HEIGHT as u32, 1);
        }

        context.encoder.copy_texture_to_texture(
            ImageCopyTexture {
                texture: &temp_target,
                mip_level: 0,
                origin: Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            ImageCopyTexture {
                texture: &a.differ.get_render_target(),
                mip_level: 0,
                origin: Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            texture_extent,
        );

        a.submit(context).await;

        if !args.common.redraw {
            break;
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
struct ShaderUniforms {
    time: f32,
}

fn main() {
    env_logger::init();
    pollster::block_on(run());
}
