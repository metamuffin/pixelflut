use clap::Parser;
use pixelflut_gpu::app::App;
use pixelflut_gpu::constants::{CANVAS_HEIGHT, CANVAS_WIDTH};
use pixelflut_gpu::font::FontRenderer;
use pixelflut_gpu::CommonArgs;
use std::time::{Duration, Instant};
use wgpu::RenderPassDescriptor;

#[derive(Parser, Debug)]
struct Args {
    #[clap(flatten)]
    common: CommonArgs,
}

async fn run() {
    let args = Args::parse();
    let mut a = App::new(&args.common).await;

    let mut font_renderer = FontRenderer::new(&a.device);

    let mut dtime = Duration::ZERO;
    let mut stime = Duration::ZERO;
    let mut fps_rounded = 0.0;
    for i in 0.. {
        let start_frame = Instant::now();
        let mut context = a.get_context();

        context.encoder.begin_render_pass(&RenderPassDescriptor {
            label: None,
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &context.target,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: true,
                },
            })],
            depth_stencil_attachment: None,
        });

        font_renderer.enqueue(wgpu_glyph::Section {
            screen_position: (10.0, 10.0),
            bounds: (CANVAS_WIDTH as f32, CANVAS_HEIGHT as f32),
            text: vec![wgpu_glyph::Text::new(
                format!(
                    "frame: {}\nfps: {:.1}\ndraw: {}µs\nsend: {}ms",
                    i,
                    fps_rounded,
                    dtime.as_micros(),
                    stime.as_millis()
                )
                .as_str(),
            )
            .with_color([1.0, 1.0, 1.0, 1.0])
            .with_scale(30.0)],
            ..wgpu_glyph::Section::default()
        });
        // font_renderer.enqueue(wgpu_glyph::Section {
        //     screen_position: (10.0, 300.0),
        //     bounds: (CANVAS_WIDTH as f32, CANVAS_HEIGHT as f32),
        //     text: vec![wgpu_glyph::Text::new(format!("wgpu go brrr",).as_str())
        //         .with_color([1.0, 0.0, 0.0, 1.0])
        //         .with_scale(100.0)],
        //     ..wgpu_glyph::Section::default()
        // });

        font_renderer.finish(&a.device, &mut context);

        a.differ
            .compute_diff(&a.queue, &mut context.encoder, &mut a.stats);
        a.queue.submit(Some(context.encoder.finish()));

        font_renderer.post_render();

        let end_render = Instant::now();
        a.differ.send_diff(&a.device, &mut a.client).await;

        let end_send = Instant::now();
        dtime = end_render - start_frame;
        stime = end_send - start_frame;
        let fps = 1.0 / stime.as_secs_f32();
        fps_rounded += (fps - fps_rounded) * 0.05;
    }
}

fn main() {
    env_logger::init();
    pollster::block_on(run());
}
