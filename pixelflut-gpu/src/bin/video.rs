use clap::Parser;
use pixelflut_gpu::app::App;
use pixelflut_gpu::video_source::VideoSource;
use pixelflut_gpu::CommonArgs;
use std::io::stdin;

#[derive(Parser, Debug)]
struct Args {
    #[clap(flatten)]
    common: CommonArgs,
}

async fn run() {
    let args = Args::parse();
    let mut app = App::new(&args.common).await;
    let video_source = VideoSource::new(Box::new(stdin()));

    loop {
        let context = app.get_context();
        video_source.write(&app.queue, app.differ.get_render_target());
        app.submit(context).await;
    }
}

fn main() {
    env_logger::init();
    pollster::block_on(run());
}
