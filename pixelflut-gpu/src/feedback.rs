use log::{debug, warn};

use crate::constants::{CANVAS_HEIGHT, CANVAS_WIDTH};
use std::{
    io::Read,
    process::{Child, Command, Stdio},
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, Mutex,
    },
};

#[derive(Debug)]
pub struct CanvasFeedback {
    pub source_uri: String,
    buffers: [Mutex<Vec<u8>>; 2],
    pub frame_count: AtomicUsize,
    ffmpeg: Mutex<Option<Child>>,
}

pub static FEEDBACK_BUF_SIZE: usize = CANVAS_HEIGHT * CANVAS_WIDTH * 4;

impl CanvasFeedback {
    pub fn new(input: &str) -> Self {
        let ffmpeg = Command::new("/bin/ffmpeg")
            .args(&[
                "-i",
                input,
                "-vf",
                "scale=1280x720,format=rgba",
                "-f",
                "rawvideo",
                "pipe:1",
            ])
            .stdout(Stdio::piped())
            .stderr(Stdio::null())
            .spawn()
            .unwrap();
        Self {
            source_uri: input.to_owned(),
            ffmpeg: Mutex::new(Some(ffmpeg)),
            buffers: [
                Mutex::new((0..FEEDBACK_BUF_SIZE as usize).map(|_| 0).collect()),
                Mutex::new((0..FEEDBACK_BUF_SIZE as usize).map(|_| 0).collect()),
            ],
            frame_count: AtomicUsize::new(0),
        }
    }
    pub fn get_cold_buffer<'a>(&'a self) -> &'a Mutex<Vec<u8>> {
        &self.buffers[self.frame_count.load(Ordering::Relaxed) % 2]
    }
    pub fn get_hot_buffer<'a>(&'a self) -> &'a Mutex<Vec<u8>> {
        &self.buffers[(self.frame_count.load(Ordering::Relaxed) + 1) % 2]
    }
    pub fn run(self: Arc<Self>) {
        let mut ffmpeg = self.ffmpeg.lock().unwrap().take().unwrap();
        let stdout = ffmpeg.stdout.as_mut().unwrap();
        loop {
            let buffer = self.get_hot_buffer();
            if let Ok(buffer) = &mut buffer.try_lock() {
                debug!(
                    "feedback frame {}",
                    self.frame_count.load(Ordering::Relaxed)
                );
                stdout.read_exact(buffer).unwrap();
            } else {
                warn!("frame skipped");
            }
            self.frame_count.fetch_add(1, Ordering::Relaxed);
        }
    }
}
