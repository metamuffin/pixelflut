use crate::constants::{CANVAS_HEIGHT, CANVAS_WIDTH};
use log::{debug, warn};
use std::{
    io::Read,
    num::NonZeroU32,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, Mutex,
    },
};
use wgpu::{Extent3d, ImageCopyTexture, ImageDataLayout, Origin3d, Queue, Texture};

pub trait VideoStream: Read + Send {}

pub struct VideoSource {
    buffers: [Mutex<Vec<u8>>; 2],
    frame_count: AtomicUsize,
}

pub static VIDEO_BUF_SIZE: usize = CANVAS_HEIGHT * CANVAS_WIDTH * 4;

impl VideoSource {
    pub fn new(input: Box<dyn Read + Sync + Send>) -> Arc<Self> {
        let this = Arc::new(Self {
            buffers: [
                Mutex::new((0..VIDEO_BUF_SIZE as usize).map(|_| 0).collect()),
                Mutex::new((0..VIDEO_BUF_SIZE as usize).map(|_| 0).collect()),
            ],
            frame_count: AtomicUsize::new(0),
        });
        let this2 = this.clone();
        std::thread::spawn(move || this2.run(input));
        return this;
    }
    pub fn get_cold_buffer<'a>(&'a self) -> &'a Mutex<Vec<u8>> {
        &self.buffers[self.frame_count.load(Ordering::Relaxed) % 2]
    }
    pub fn get_hot_buffer<'a>(&'a self) -> &'a Mutex<Vec<u8>> {
        &self.buffers[(self.frame_count.load(Ordering::Relaxed) + 1) % 2]
    }
    pub fn run(self: Arc<Self>, mut source: impl Read) {
        loop {
            let buffer = self.get_hot_buffer();
            if let Ok(buffer) = &mut buffer.try_lock() {
                self.frame_count.fetch_add(1, Ordering::Relaxed);
                debug!("video frame {}", self.frame_count.load(Ordering::Relaxed));
                source.read_exact(buffer).unwrap();
            } else {
                warn!("frame skipped");
            }
        }
    }

    pub fn write(self: &Arc<Self>, queue: &Queue, target: &Texture) {
        let buffer = self.get_cold_buffer();
        let buffer = buffer.lock().unwrap();
        debug!("copied feedback into previous framebuffer");
        queue.write_texture(
            ImageCopyTexture {
                texture: &target,
                mip_level: 0,
                origin: Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            &buffer,
            ImageDataLayout {
                offset: 0,
                bytes_per_row: Some((CANVAS_WIDTH * 4) as u32),
                rows_per_image: None,
            },
            Extent3d {
                width: CANVAS_WIDTH as u32,
                height: CANVAS_HEIGHT as u32,
                depth_or_array_layers: 1,
            },
        );
    }
}
