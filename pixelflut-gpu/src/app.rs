use crate::{client::PixelflutClient, diff::Differ, stats::Stats, CommonArgs, DrawContext};
use log::debug;
use std::time::Instant;
use wgpu::{Adapter, Backends, Device, Instance, Limits, Queue};

pub struct App {
    pub args: CommonArgs,
    pub instance: Instance,
    pub adapter: Adapter,
    pub queue: Queue,
    pub device: Device,
    pub differ: Differ,
    pub client: PixelflutClient,
    pub stats: Stats,

    pub frame: usize,
    pub start: Instant,
}

impl App {
    pub async fn new(args: &CommonArgs) -> Self {
        let args = args.to_owned();
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: Backends::all(),
            dx12_shader_compiler: Default::default(),
        });
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions::default())
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::ADDRESS_MODE_CLAMP_TO_BORDER,
                    limits: Limits {
                        max_texture_dimension_2d: 4096,
                        ..Default::default()
                    },
                },
                None,
            )
            .await
            .unwrap();

        let client = PixelflutClient::new(
            args.addr
                .clone()
                .unwrap_or_else(|| std::env::var("PIXELFLUT_SERVER_ADDR").unwrap())
                .as_str(),
        );
        let differ = Differ::from_args(&device, &args);

        Self {
            stats: Stats::new(args.stats, &device),
            frame: 0,
            start: Instant::now(),
            args,
            adapter,
            device,
            instance,
            queue,
            client,
            differ,
        }
    }

    pub fn get_context(&self) -> DrawContext {
        debug!("encode commands");
        DrawContext {
            encoder: self
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("draw"),
                }),
            target: self.differ.get_render_target_view(),
        }
    }

    pub async fn submit(&mut self, mut context: DrawContext) {
        if self.args.stats > 0 {
            self.stats.checkpoint(Some("stats"));
            Stats::update_in(self);
            self.stats.draw(&mut context, &self.device)
        }
        debug!("submit");

        self.differ
            .compute_diff(&self.queue, &mut context.encoder, &mut self.stats);

        self.stats.checkpoint(Some("gpu submit queue"));
        let command_buffer = context.encoder.finish();
        self.queue.submit(Some(command_buffer));

        self.stats.checkpoint(Some("send changes"));
        self.stats.changes_sent = self.differ.send_diff(&self.device, &mut self.client).await;

        self.stats.checkpoint(Some("staging belt"));
        if self.args.stats > 0 {
            self.stats.post_render()
        }

        self.stats.checkpoint(Some("fps limit"));

        self.frame += 1;
        if let Some(fpslimit) = self.args.fpslimit {
            let until_end = self.frame as f64 / fpslimit - self.start.elapsed().as_secs_f64();
            if until_end > 0.0 {
                std::thread::sleep(std::time::Duration::from_secs_f64(until_end));
            }
        }

        self.stats.checkpoint(Some("other"));
    }
}
