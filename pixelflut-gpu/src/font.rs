use wgpu::{util::StagingBelt, Device};
use wgpu_glyph::{GlyphBrush, GlyphBrushBuilder};

use crate::{
    constants::{CANVAS_HEIGHT, CANVAS_WIDTH, RENDER_FORMAT},
    DrawContext,
};

pub struct FontRenderer {
    staging_belt: StagingBelt,
    // local_pool: LocalPool,
    // local_spawner: LocalSpawner,
    glyph_brush: GlyphBrush<()>,
}

impl FontRenderer {
    pub fn new(device: &Device) -> Self {
        let font = wgpu_glyph::ab_glyph::FontArc::try_from_slice(include_bytes!(
            "/usr/share/fonts/noto/NotoSans-Regular.ttf"
        ))
        .unwrap();

        let glyph_brush = GlyphBrushBuilder::using_font(font).build(&device, RENDER_FORMAT);
        let staging_belt = wgpu::util::StagingBelt::new(1024);
        // let local_pool = futures::executor::LocalPool::new();
        // let local_spawner = local_pool.spawner();

        Self {
            glyph_brush,
            // local_pool,
            // local_spawner,
            staging_belt,
        }
    }

    pub fn enqueue(&mut self, section: wgpu_glyph::Section) {
        self.glyph_brush.queue(section)
    }

    pub fn finish(&mut self, device: &Device, c: &mut DrawContext) {
        self.glyph_brush
            .draw_queued(
                device,
                &mut self.staging_belt,
                &mut c.encoder,
                &c.target,
                CANVAS_WIDTH as u32,
                CANVAS_HEIGHT as u32,
            )
            .expect("Draw queued");
        self.staging_belt.finish();
    }

    pub fn post_render(&mut self) {
        self.staging_belt.recall();
        // self.local_spawner
        //     .spawn()
        //     .expect("Recall staging belt");

        // self.local_pool.run_until_stalled();
    }
}
