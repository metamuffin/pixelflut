use log::debug;
use std::{
    collections::BTreeMap,
    time::{Duration, Instant},
};
use wgpu::Device;

use crate::{
    app::App,
    constants::{CANVAS_HEIGHT, CANVAS_WIDTH},
    font::FontRenderer,
    DrawContext,
};

pub struct Stats {
    pub level: usize,
    pub timing: BTreeMap<&'static str, Duration>,
    pub current: Option<(&'static str, Instant)>,
    font_renderer: FontRenderer,
    display: String,

    pub changes_sent: usize,
    pub frame: usize,

    pub frame_start: Instant,
    pub smooth_dtime: f64,
}

impl Stats {
    pub fn new(level: usize, device: &Device) -> Self {
        Self {
            display: String::new(),
            level,
            timing: BTreeMap::new(),
            current: Some(("", Instant::now())),
            font_renderer: FontRenderer::new(device),
            changes_sent: 0,
            frame: 0,
            frame_start: Instant::now(),
            smooth_dtime: 0.0,
        }
    }

    pub fn checkpoint(&mut self, name: Option<&'static str>) {
        if self.level < 2 {
            return;
        }

        debug!("{:?}", name);
        let now = Instant::now();
        if let Some((name, start)) = self.current {
            self.timing.insert(name, now - start);
        }

        if let Some(name) = name {
            self.current = Some((name, now));
        }
    }

    pub fn update_in(app: &mut App) {
        let this = &mut app.stats;

        let mut s = format!(
            "{} {} (https://git.metamuffin.org/pixelflut)\nflags: {}\n",
            env!("CARGO_PKG_NAME"),
            env!("CARGO_PKG_VERSION"),
            std::env::args().skip(1).collect::<Vec<_>>().join(" ")
        );
        if let Some(f) = &app.differ.feedback {
            s += format!("feedback stream: {}\n", f.source_uri).as_str()
        }
        {
            let info = app.adapter.get_info();
            s += format!(
                "wgpu backend: {:?}; {:?}; {}\n",
                info.backend, info.device_type, info.name
            )
            .as_str();
        }

        s += format!("changes sent: {}\n", this.changes_sent).as_str();
        s += format!("frame: {}\n", this.frame).as_str();
        s += format!(
            "fps: {:.1} ({:.2}ms)\n",
            1.0 / this.smooth_dtime,
            this.smooth_dtime * 1000.0
        )
        .as_str();
        this.frame += 1;

        let now = Instant::now();
        let delta = (now - this.frame_start).as_secs_f64();
        this.frame_start = now;
        this.smooth_dtime += (delta - this.smooth_dtime) * 0.01;

        for (k, v) in &this.timing {
            s += format!("timing, {}: {}µs\n", k, v.as_micros()).as_str()
        }

        app.stats.display = s;
    }

    pub fn draw(&mut self, context: &mut DrawContext, device: &Device) {
        self.font_renderer.enqueue(wgpu_glyph::Section {
            screen_position: (0.0, 0.0),
            bounds: (CANVAS_WIDTH as f32, CANVAS_HEIGHT as f32),
            text: vec![wgpu_glyph::Text::new(self.display.as_str())
                .with_color([0.9, 0.3, 0.8, 1.0])
                .with_scale(18.0)],
            ..wgpu_glyph::Section::default()
        });

        self.font_renderer.finish(device, context);
        self.timing.clear();
    }
    pub fn post_render(&mut self) {
        self.font_renderer.post_render();
    }
}
