use std::{
    io::{BufWriter, Write},
    net::TcpStream,
};

pub struct PixelflutClient {
    connection: BufWriter<TcpStream>,
}

impl PixelflutClient {
    pub fn new(addr: &str) -> Self {
        Self {
            connection: BufWriter::new(TcpStream::connect(addr).unwrap()),
        }
    }

    pub fn pixel(&mut self, x: usize, y: usize, r: u8, g: u8, b: u8) {
        write!(
            self.connection,
            "PX {} {} {:02x}{:02x}{:02x}\n",
            x, y, r, g, b
        )
        .unwrap()
    }
}
