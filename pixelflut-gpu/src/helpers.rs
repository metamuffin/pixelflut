use std::{fs::File, io::Write};
use futures::channel::oneshot;
use wgpu::{Buffer, Device};

pub async fn create_png(
    png_output_path: &str,
    device: &Device,
    output_buffer: &Buffer,
    buffer_dimensions: &BufferDimensions,
) {
    let buffer_slice = output_buffer.slice(..);
    let (tx, rx) = oneshot::channel();
    buffer_slice.map_async(wgpu::MapMode::Read, move |_| tx.send(()).unwrap());

    device.poll(wgpu::Maintain::Wait);
    rx.await.unwrap();

    let padded_buffer = buffer_slice.get_mapped_range();

    let mut png_encoder = png::Encoder::new(
        File::create(png_output_path).unwrap(),
        buffer_dimensions.width as u32,
        buffer_dimensions.height as u32,
    );
    png_encoder.set_depth(png::BitDepth::Eight);
    png_encoder.set_color(png::ColorType::RGBA);
    let mut png_writer = png_encoder
        .write_header()
        .unwrap()
        .into_stream_writer_with_size(buffer_dimensions.unpadded_bytes_per_row);

    // from the padded_buffer we write just the unpadded bytes into the image
    for chunk in padded_buffer.chunks(buffer_dimensions.padded_bytes_per_row) {
        png_writer
            .write_all(&chunk[..buffer_dimensions.unpadded_bytes_per_row])
            .unwrap();
    }
    png_writer.finish().unwrap();
    output_buffer.unmap();
}
pub struct BufferDimensions {
    pub width: usize,
    pub height: usize,
    pub unpadded_bytes_per_row: usize,
    pub padded_bytes_per_row: usize,
}

impl BufferDimensions {
    pub fn new(width: usize, height: usize) -> Self {
        let bytes_per_pixel = std::mem::size_of::<u32>();
        let unpadded_bytes_per_row = width * bytes_per_pixel;
        let align = wgpu::COPY_BYTES_PER_ROW_ALIGNMENT as usize;
        let padded_bytes_per_row_padding = (align - unpadded_bytes_per_row % align) % align;
        let padded_bytes_per_row = unpadded_bytes_per_row + padded_bytes_per_row_padding;
        Self {
            width,
            height,
            unpadded_bytes_per_row,
            padded_bytes_per_row,
        }
    }
}
