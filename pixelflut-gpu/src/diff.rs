use crate::{
    client::PixelflutClient,
    constants::{CANVAS_HEIGHT, CANVAS_WIDTH, RENDER_FORMAT},
    feedback::CanvasFeedback,
    helpers::{create_png, BufferDimensions},
    stats::Stats,
    CommonArgs,
};
use bytemuck::{Pod, Zeroable};
use futures::channel::oneshot;
use log::{debug, info};
use rand::prelude::SliceRandom;
use std::{borrow::Cow, num::NonZeroU32, sync::Arc};
use wgpu::{
    util::DeviceExt, BindGroup, Buffer, CommandEncoder, ComputePipeline, Device, Extent3d,
    ImageCopyTexture, ImageDataLayout, Origin3d, Queue, Texture, TextureView,
};

pub struct Differ {
    pub frame_index: usize,
    pub shuffle_dispatch: bool,
    pub skip_diff: bool,

    pub feedback: Option<Arc<CanvasFeedback>>,

    texture_extent: Extent3d,
    render_textures: Vec<Texture>,
    diff_texture: Texture,
    output_buffer: Buffer,

    bind_group: BindGroup,
    buffer_dimensions: BufferDimensions,
    pipeline: ComputePipeline,

    uniform_buffer: Buffer,
    uniforms: DifferUniforms,
}

impl Differ {
    pub fn from_args(device: &Device, args: &CommonArgs) -> Self {
        let mut d = Differ::new(device);
        d.skip_diff = args.nodiff;
        d.shuffle_dispatch = args.shuffle;
        if let Some(u) = args.feedback.as_ref() {
            d.enable_feedback(u);
        }
        if args.auto_feedback {
            d.enable_feedback(std::env::var("PIXELFLUT_FEEDBACK_URI").unwrap().as_str());
        }
        d.uniforms.similarity = args.similarity;
        d
    }

    pub fn new(device: &Device) -> Differ {
        let buffer_dimensions = BufferDimensions::new(CANVAS_WIDTH, CANVAS_HEIGHT);

        let output_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: (buffer_dimensions.padded_bytes_per_row * buffer_dimensions.height) as u64,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });

        let texture_extent = wgpu::Extent3d {
            width: buffer_dimensions.width as u32,
            height: buffer_dimensions.height as u32,
            depth_or_array_layers: 1,
        };

        let render_textures = (0..2)
            .map(|_| {
                device.create_texture(&wgpu::TextureDescriptor {
                    size: texture_extent,
                    view_formats: &[],
                    mip_level_count: 1,
                    sample_count: 1,
                    dimension: wgpu::TextureDimension::D2,
                    format: RENDER_FORMAT,
                    usage: wgpu::TextureUsages::RENDER_ATTACHMENT
                        | wgpu::TextureUsages::COPY_SRC
                        | wgpu::TextureUsages::COPY_DST
                        | wgpu::TextureUsages::STORAGE_BINDING
                        | wgpu::TextureUsages::TEXTURE_BINDING,
                    label: None,
                })
            })
            .collect::<Vec<_>>();

        let diff_texture = device.create_texture(&wgpu::TextureDescriptor {
            size: texture_extent,
            view_formats: &[],
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: RENDER_FORMAT,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT
                | wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::STORAGE_BINDING
                | wgpu::TextureUsages::TEXTURE_BINDING,
            label: None,
        });

        let shader_module = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(include_str!("shaders/diff.wgsl"))),
        });

        let pipeline = device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
            label: Some("diff"),
            layout: None,
            module: &shader_module,
            entry_point: "main",
        });

        let uniforms = DifferUniforms {
            similarity: 0.3,
            swap: 0,
        };

        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("diff uniform buffer"),
            contents: bytemuck::cast_slice(&[uniforms]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Texture bind group"),
            layout: &pipeline.get_bind_group_layout(0),
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: uniform_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::TextureView(
                        &render_textures[0].create_view(&wgpu::TextureViewDescriptor::default()),
                    ),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: wgpu::BindingResource::TextureView(
                        &render_textures[1].create_view(&wgpu::TextureViewDescriptor::default()),
                    ),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: wgpu::BindingResource::TextureView(
                        &diff_texture.create_view(&wgpu::TextureViewDescriptor::default()),
                    ),
                },
            ],
        });

        Differ {
            diff_texture,
            output_buffer,
            render_textures,
            bind_group,
            uniform_buffer,
            pipeline,
            buffer_dimensions,
            uniforms,
            texture_extent,
            shuffle_dispatch: false,
            skip_diff: false,
            feedback: None,
            frame_index: 0,
        }
    }

    pub fn get_render_target<'a>(&'a self) -> &'a Texture {
        &self.render_textures[self.frame_index % 2]
    }

    pub fn get_render_target_view(&self) -> TextureView {
        self.render_textures[self.frame_index % 2]
            .create_view(&wgpu::TextureViewDescriptor::default())
    }

    pub fn enable_feedback(&mut self, feedback_source: &str) {
        let f = Arc::new(CanvasFeedback::new(feedback_source));
        let f2 = f.clone();
        std::thread::spawn(move || f2.run());
        self.feedback = Some(f);
    }

    pub fn compute_diff(&mut self, queue: &Queue, encoder: &mut CommandEncoder, stats: &mut Stats) {
        if let Some(feedback) = self.feedback.clone() {
            stats.checkpoint(Some("lock cold buffer"));
            let buffer = feedback.get_cold_buffer();
            let buffer = buffer.lock().unwrap();
            stats.checkpoint(Some("upload feedback"));
            debug!("copied feedback into previous framebuffer");
            queue.write_texture(
                ImageCopyTexture {
                    texture: &self.render_textures[(self.frame_index + 1) % 2],
                    mip_level: 0,
                    origin: Origin3d::ZERO,
                    aspect: wgpu::TextureAspect::All,
                },
                &buffer,
                ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some((CANVAS_WIDTH * 4) as u32),
                    rows_per_image: None,
                },
                self.texture_extent,
            );
        }

        if !self.skip_diff {
            stats.checkpoint(Some("compute pass"));
            self.uniforms.swap = (self.frame_index % 2) as i32;
            queue.write_buffer(
                &self.uniform_buffer,
                0,
                bytemuck::cast_slice(&[self.uniforms]),
            );
            let mut pass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
                label: Some("diff"),
            });
            pass.set_pipeline(&self.pipeline);
            pass.set_bind_group(0, &self.bind_group, &[]);
            pass.dispatch_workgroups(CANVAS_WIDTH as u32, CANVAS_HEIGHT as u32, 1)
        }

        stats.checkpoint(Some("download diff"));
        encoder.copy_texture_to_buffer(
            match self.skip_diff {
                true => self.render_textures[self.frame_index % 2].as_image_copy(),
                false => self.diff_texture.as_image_copy(),
            },
            wgpu::ImageCopyBuffer {
                buffer: &self.output_buffer,
                layout: wgpu::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(self.buffer_dimensions.padded_bytes_per_row as u32),
                    rows_per_image: None,
                },
            },
            self.texture_extent,
        );
        self.frame_index += 1;
    }

    pub async fn send_diff(&self, device: &Device, client: &mut PixelflutClient) -> usize {
        let buffer_slice = self.output_buffer.slice(..);

        let (tx, rx) = oneshot::channel();
        buffer_slice.map_async(wgpu::MapMode::Read, move |_| tx.send(()).unwrap());
        let mut change_count = 0;

        // TODO dont block here!
        device.poll(wgpu::Maintain::Wait);

        rx.await.unwrap();

        {
            let padded_buffer = buffer_slice.get_mapped_range();
            let mut changes = vec![];
            for (y, chunk) in padded_buffer
                .chunks(self.buffer_dimensions.padded_bytes_per_row)
                .enumerate()
            {
                let row = &chunk[..self.buffer_dimensions.unpadded_bytes_per_row];
                for (x, color) in row.chunks_exact(4).enumerate() {
                    if color[3] != 0 {
                        change_count += 1;
                        if self.shuffle_dispatch {
                            changes.push((x, y, color[0], color[1], color[2]))
                        } else {
                            client.pixel(x, y, color[0], color[1], color[2]);
                        }
                    }
                }
            }
            if self.shuffle_dispatch {
                changes.shuffle(&mut rand::thread_rng());
                for (x, y, r, g, b) in changes {
                    client.pixel(x, y, r, g, b);
                }
            }
            info!("sent changes: {}", change_count);
        }
        self.output_buffer.unmap();
        change_count
    }

    pub async fn dump_to_file(&self, device: &Device) {
        create_png(
            format!("./temp/frame-{}.png", self.frame_index).as_str(),
            &device,
            &self.output_buffer,
            &self.buffer_dimensions,
        )
        .await;
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
pub struct DifferUniforms {
    swap: i32,
    similarity: f32,
}
