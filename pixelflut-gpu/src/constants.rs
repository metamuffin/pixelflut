use wgpu::TextureFormat;

pub static RENDER_FORMAT: TextureFormat = wgpu::TextureFormat::Rgba8Unorm;
pub static CANVAS_WIDTH: usize = 1280;
pub static CANVAS_HEIGHT: usize = 720;
